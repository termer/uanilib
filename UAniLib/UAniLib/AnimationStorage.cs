﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using AniLib;
using UnityEngine;

namespace UAniLib
{
    [Serializable]
    public class AnimationElement
    {
        public string Name;
        public TextAsset Data;
    }

    public class AnimationStorage:MonoBehaviour
    {
        public List<AnimationElement> AnimationsData;

        private Dictionary<string, AniLib.Animations.Animation> mAnimations; 

        private void Awake()
        {
            mAnimations = new Dictionary<string, AniLib.Animations.Animation>();
            CreateAnimationsFromData();
        }

        public AniLib.Animations.Animation GetAnimation(string name)
        {
            return mAnimations[name];
        }

        private void CreateAnimationsFromData()
        {
            if (!CheckAnimationData()) return;
            foreach (AnimationElement element in AnimationsData)
            {
                LoadElement(element);
            }
        }

        private void LoadElement(AnimationElement element)
        {
            if (!CheckElement(element)) return;

            AniLib.Animations.Animation animationFromStream = LoadFromStream(element);

            mAnimations.Add(element.Name, animationFromStream);
        }

        private AniLib.Animations.Animation LoadFromStream(AnimationElement element)
        {
            Stream stream = new MemoryStream(element.Data.bytes);
            AniLib.Animations.Animation fromStream = Utilites.DesiarilizeFromStream<AniLib.Animations.Animation>(stream);
            return fromStream;
        }

        private bool CheckAnimationData()
        {
            if (AnimationsData == null ||
                AnimationsData.Count == 0)
            {
                Debug.LogWarning("Empty animation data");
                return false;
            }
            return true;
        }

        private bool CheckElement(AnimationElement element)
        {
            if (string.IsNullOrEmpty(element.Name))
            {
                Debug.LogError("Empty animation name");
                return false;
            }
            if (mAnimations.ContainsKey(element.Name))
            {
                Debug.LogError("Already has animation with such name");
                return false;
            }
            return true;
        }
    }
}
