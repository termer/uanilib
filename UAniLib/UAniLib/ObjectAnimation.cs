﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AniLib.Animations;
using UnityEngine;

namespace UAniLib
{
    public class ObjectAnimation:MonoBehaviour
    {
        private const float cScale = 0.02f;

        public List<GameObject> GameObjectElements;

        public string DefaultAnimation;

        private AnimationStorage mAnimationStorage;


        public bool Playing = true;

        private AniLib.Animations.Animation Animation;

        private float mElapsed;

        private void Start()
        {
            GameObject animationStorage = GameObject.Find("AnimationStorage");
            mAnimationStorage = animationStorage.GetComponent<AnimationStorage>();


            if (!string.IsNullOrEmpty(DefaultAnimation))
            {
                SetAnimationTo(DefaultAnimation);
            }
        }

        public void SetAnimationTo(string name)
        {
            Animation = mAnimationStorage.GetAnimation(name);
        }

        private void Update()
        {
            if (Animation == null)
            {
                return;
            }

            if (Playing)
            {
                PlayAnimation();
                SetElementsPositions();
            }
        }

        private void SetElementsPositions()
        {
            for (int index = 0; index < Animation.Elements.Count; index++)
            {
                Element element = Animation.Elements[index];
                GameObject gameObject = GameObjectElements[index];

                gameObject.transform.localPosition = new Vector3(element.Position.X * cScale + element.CurrentStatus.Position.X * cScale,
                    element.Position.Y + element.CurrentStatus.Position.Y * cScale,
                    gameObject.transform.localPosition.z);
            }
        }

        private void PlayAnimation()
        {
            mElapsed += Time.deltaTime;
            Animation.InterpolateElementsTo(mElapsed);
        }
    }
}
